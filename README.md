Flaskex

Установка и запуск приложения осуществляется на Ubuntu 20.04

Исходники находятся https://github.com/anfederico/flaskex

Для работы приложения нам потребуется установить python3 и pip3:

sudo apt install python3-minimal
python3 --version
sudo apt install python3-pip
pip3 install --user --upgrade pip

Далее следуем клонируем репозиторий:

git clone https://github.com/anfederico/Flaskex

устанавливаем зависимости:

cd Flaskex

pip install -r requirements.txt

запускаем с помощью:

python app.py

При появлении ошибки вида:

AttributeError: module 'wtforms.validators' has no attribute 'required'

необходимо изменить в файле /scripts/forms.py:

 validators.required() на validators.DataRequired()

Пробуем снова запустить приложение:

$ python3 app.py
 
Открываем в браузере нужный хост и порт и видим запущенное приложение
 

Далее завернем приложение в контейнер для более легкого запуска в будущем (я использую хранилище Gitlab)

docker build -t registry.gitlab.com/my-group6431413/myproject/flaskex .

И проверим, что контейнер корректно запускается:

docker run -p 1234:5000 registry.gitlab.com/my-group6431413/myproject/flaskex:latest
И загружаем его в Gitlab regestry:

docker push registry.gitlab.com/my-group6431413/myproject/flaskex

Далее заворачиваем наш контейнер в docker-compose.yml и запускаем:

docker compose up –d

Проверяем в браузере что все работает на указанном порту

